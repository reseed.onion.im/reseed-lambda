/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * ReturnObject.java - Return object
 * Copyright (C) 2025 Chris Barry <chris@barry.im>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package im.barry.reseed;

import java.io.Serializable;

public class ReturnObject implements Serializable {
    private static final long serialVersionUID = -8488406175557715752L;
    private String reseed;

    private String status;

    public ReturnObject(String status, String reseed) {
        this.reseed = reseed;
        this.status = status;
    }

    public String getReseed() { return reseed; }

    public void setReseed(String reseed) {
        this.reseed = reseed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
