/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * ReseedHealthChecker.java - Entrypoint for Lambda handler
 * Copyright (C) 2025 Chris Barry <chris@barry.im>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package im.barry.reseed;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import net.i2p.crypto.SU3File;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class ReseedHealthChecker implements RequestHandler<List<String>, List<ReturnObject>> {
    private static final int MAX_AGE = 60 * 60 * 24 * 7;

    private final HttpClient client;

    public ReseedHealthChecker() {
        client = HttpClient.newHttpClient();
    }

    public static HttpRequest a(String reseed) {
        return HttpRequest.newBuilder()
                .GET()
                .version(HttpClient.Version.HTTP_2)
                .header("User-Agent", "Wget/1.11.4")
                .uri(URI.create(reseed))
                .timeout(Duration.ofSeconds(15))
                .build();
    }

    // Shamelessly stolen from https://github.com/i2p/i2p.i2p/blob/master/core/java/src/net/i2p/crypto/SU3File.java#L729
    public static boolean isFresh(final String signedFile) {
        long now = Instant.now().getEpochSecond();
        long ver = 0;
        SU3File file = new SU3File(signedFile);
        file.setVerifySignature(false);
        try {
            String versionString = file.getVersionString();
            if (versionString.equals("")) {
                //logger.log("No version string found in file " + signedFile);
                return false;
            }

            ver = Long.parseLong(versionString);
            if (ver > 1000000000L) {
                //logger.log("Version: "+ versionString +" " + DataHelper.formatTime(ver * 1000));
            } else {
                //logger.log("Version: "+ versionString);
            }
            //logger.log("now: " + now + " ver: " + ver + " diff: " + String.valueOf(now - ver));
            return now - ver < MAX_AGE;
        } catch (Exception e) {
            //logger.log("error: "+ e);
            return false;
        }
    }

    @Override
    public List<ReturnObject> handleRequest(List<String> reseeds, final Context context) {
        LambdaLogger logger = context.getLogger();

        List<ReturnObject> x = new ArrayList<>();
        for (String reseed : reseeds) {
            try {
                logger.log("Starting GET for " + reseed);

                HttpResponse<Path> response = client.send(ReseedHealthChecker.a(reseed), HttpResponse.BodyHandlers.ofFile(Paths.get(String.format("/tmp/{}.su3", reseed))));

                logger.log("Finished GET with " + response.statusCode());

                if (429 == response.statusCode()) {
                    logger.log("Hit rate limit");
                    x.add(new ReturnObject("Hit rate limit", reseed));
                    continue;
                    //throw new ReseedRateLimit("Hit rate limit!");
                }

                if (200 != response.statusCode()) {
                    logger.log("General HTTP error");
                    x.add(new ReturnObject("General HTTP error", reseed));
                    continue;
                    //throw new ReseedGeneralError("General HTTP error");
                }

                logger.log("Starting checks");

                if (!ReseedHealthChecker.isFresh(String.format("/tmp/{}.su3", reseed))) {
                    logger.log("Reseed file is too old");
                    x.add(new ReturnObject("Reseed file is too old", reseed));
                    continue;
                    //throw new ReseedTooOldException("File is too old!");
                }

                logger.log("Finished checks, all ok");
                x.add(new ReturnObject("OK", reseed));
            } catch (Exception e) {
                logger.log("Finished checks, all bad");
                logger.log(""+ e);
                throw new RuntimeException("General error go debug!");
            }
        }
        return x;
    }
}
