# App

A simple lambda to alert me if my I2P reseed server is out of date.

## TODO
- Email via SNS is not working
- Better logging, slf4j isn't working

## Resources
- arn:aws:lambda:us-east-n:x:function:reseed
- arn:aws:logs:us-east-n:x:log-group:/aws/lambda/reseed:*
- arn:aws:events:us-east-x:x:rule/reseed-cron
- arn:aws:sns:us-east-n:x:reseed-error
- arn:aws:iam::x:role/service-role/reseed-role-n

## Prerequisites
- Java 17+
- Apache Maven

#### Building the project
```
mvn clean install
```

## License
This project is licensed under the **GNU General Public License v3.0**.
You are free to use, modify, and distribute it under the terms of the GPLv3 or later.

See the [LICENSE](LICENSE) file for details.

For more information, visit: [https://www.gnu.org/licenses/gpl-3.0.html](https://www.gnu.org/licenses/gpl-3.0.html).

